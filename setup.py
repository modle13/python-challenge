"""Setuptools configuration."""
import os

from setuptools import find_packages, setup

HERE = os.path.dirname(os.path.abspath(__file__))


setup(
    name="Team Sales Report",
    description="A report tool for generating team sales reports from input data.",
    license='',
    author="Matthew Odle",
    author_email="odle.matthew@gmail.com",
    version='0.1.0',
    packages=find_packages(),
    scripts=['bin/report'],
    include_package_data=True,
    zip_safe=False,
)
