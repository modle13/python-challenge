"""Unit Tests for Data Cruncher module"""
import os
from unittest import TestCase
from unittest.mock import patch
import sys

from team_sales_report import data_cruncher as test_module


class Args(object):
    def __init__(self, dictionary):
        self.__dict__.update(dictionary)
    def __str__(self):
        out = 'params:'
        class_dict = self.__dict__
        keys = list(class_dict.keys())
        for entry in keys:
            out = f'{out}\n{"":<4}{entry}: {class_dict[entry]}'
        return out

class DataCruncherTestCase(TestCase):
    def setUp(self):
        self.bare_args = Args({})

    @patch('team_sales_report.data_cruncher.report_by_product')
    @patch('team_sales_report.data_cruncher.write_json_to_csv_file')
    @patch('team_sales_report.data_cruncher.report_by_team')
    @patch('team_sales_report.data_cruncher.map_product_data_to_sales_data')
    @patch('team_sales_report.data_cruncher.read_csv_input_file')
    def test_crunch_reports(self, read_mock, map_mock, team_mock, write_mock, product_mock):
        """
        The function under test is purely function calls, with no data operational logic.

        This makes it clearer that the test is obviously correct and enables simpler tests for later logic.
        """
        args = self.bare_args
        args.t = 'teams_file'
        args.s = 'sales_file'
        args.p = 'product_file'
        args.team_report = 'team_report_file'
        args.product_report = 'product_report_file'

        test_team_data = 'team data not important for purposes of test'
        test_sales_data = 'sales data not important for purposes of test'
        test_product_data = 'product data not important for purposes of test'
        test_team_report_data = 'team report data not important for purposes of test'
        test_product_report_data = 'product report data not important for purposes of test'

        read_mock.side_effect = (test_team_data, test_sales_data, test_product_data)
        team_mock.return_value = test_team_report_data
        product_mock.return_value = test_product_report_data

        test_module.crunch_reports(args)

        read_mock.assert_any_call(args.t, 'teams')
        read_mock.assert_any_call(args.s, 'sales')
        read_mock.assert_any_call(args.p, 'products')
        map_mock.assert_called_once_with(test_product_data, test_sales_data)
        team_mock.assert_called_once_with(test_team_data, test_sales_data)
        write_mock.assert_any_call(args.team_report, test_team_report_data)
        # sales data has been mapped with product data
        product_mock.assert_called_once_with(test_sales_data)
        write_mock.assert_any_call(args.product_report, test_product_report_data)

    def test_map_product_data_to_sales_data_succeeds(self):
        test_product_id = 1
        test_lot_size = 1
        test_price = 1
        test_quantity = 2
        test_discount = 2
        test_name = 'MyProduct'
        test_lot_price = int(test_lot_size) * float(test_price)
        test_ext_price = float(test_lot_price) * float(test_quantity)
        test_ext_discount = float(test_ext_price) * float(test_discount) / 100.0
        test_product_data = [
            {
                'ProductId': test_product_id,
                'LotSize': test_lot_size,
                'Price': test_price,
                'Name': test_name,
            },
        ]

        test_sales_data = [
            {
                'ProductId': test_product_id,
                'Discount': test_discount,
                'Quantity': test_quantity,
            },
        ]
        expected = [
            {
                'ProductId': test_product_id,
                'LotSize': test_lot_size,
                'Price': test_price,
                'Name': test_name,
                'Discount': test_discount,
                'Quantity': test_quantity,
                'LotPrice': test_lot_price,
                'ExtPrice': test_ext_price,
                'ExtDiscount': test_ext_discount,
            },
        ]

        test_module.map_product_data_to_sales_data(test_product_data, test_sales_data)

        # test_sales_data should be updated with the new fields
        self.assertEqual(expected, test_sales_data)

    def test_report_by_team_succeeds(self):
        test_team_name = 'MyTeam'
        test_team_data = [
            {
                'TeamId': 1,
                'Name': test_team_name,
            },
        ]
        test_sales_data = [
            {
                'ExtPrice': 5,
                'TeamId': 1,
            },
        ]
        expected = [
            {
                'Team': test_team_name,
                'GrossRevenue': '5.00',
            },
        ]

        result = test_module.report_by_team(test_team_data, test_sales_data)

        self.assertEqual(expected, result)

    def test_report_by_product_succeeds(self):
        test_name_1 = 'MyProduct'
        test_name_2 = 'MyOtherProduct'
        test_sales_data = [
            {
                'Name': test_name_1,
                'ExtPrice': 2,
                'Quantity': 2,
                'ExtDiscount': 0.2,
            },
            {
                'Name': test_name_1,
                'ExtPrice': 5,
                'Quantity': 5,
                'ExtDiscount': 0.2,
            },
            {
                'Name': test_name_2,
                'ExtPrice': 1,
                'Quantity': 5,
                'ExtDiscount': 0.1,
            },
        ]

        expected = [
            {
                'Name': test_name_1,
                'GrossRevenue': '7.00',
                'TotalUnits': 7,
                'DiscountCost': '0.40',
            },
            {
                'Name': test_name_2,
                'GrossRevenue': '1.00',
                'TotalUnits': 5,
                'DiscountCost': '0.10',
            },
        ]

        result = test_module.report_by_product(test_sales_data)

        self.assertEqual(expected, result)
