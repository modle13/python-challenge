"""Unit Tests for Team Sales Report module"""
import json
import os
from unittest import TestCase
from unittest.mock import Mock, patch
import sys

from team_sales_report import main as test_module


class Args(object):
    def __init__(self, dictionary):
        self.__dict__.update(dictionary)
    def __str__(self):
        out = 'params:'
        class_dict = self.__dict__
        keys = list(class_dict.keys())
        for entry in keys:
            out = f'{out}\n{"":<4}{entry}: {class_dict[entry]}'
        return out

class MainTestCase(TestCase):
    def setUp(self):
        self.bare_args = Args({
            'command_name': 'some_command',
            'some_var': 'not important',
        })

    def test_define_parser(self):
        parser = test_module.define_parser()
        expected_description = 'Report team sales performance.'
        self.assertEqual(expected_description, parser.description)

    @patch('team_sales_report.main.define_parser')
    def test_parse_args_succeeds(self, define_mock):
        # get actual return
        actual = test_module.parse_args()

        # assert results
        define_mock.assert_called()

    @patch('team_sales_report.main.crunch_reports')
    @patch('team_sales_report.main.configure_logging')
    @patch('team_sales_report.main.parse_args')
    def test_run_succeeds(self, parse_mock, logging_mock, crunch_mock):
        parse_mock.return_value = self.bare_args

        test_module.run()

        parse_mock.assert_called_once()
        logging_mock.assert_called_once_with(self.bare_args)
        crunch_mock.assert_called_once_with(self.bare_args)
