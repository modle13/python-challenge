"""Unit Tests for CSV Operations module"""
import csv
import json
import os
from unittest import TestCase
from unittest.mock import patch
import sys

from team_sales_report import csv_operations as test_module


class Args(object):
    def __init__(self, dictionary):
        self.__dict__.update(dictionary)
    def __str__(self):
        out = 'params:'
        class_dict = self.__dict__
        keys = list(class_dict.keys())
        for entry in keys:
            out = f'{out}\n{"":<4}{entry}: {class_dict[entry]}'
        return out

class CSVOperationsTestCase(TestCase):
    def setUp(self):
        self.bare_args = Args({
            'command_name': 'some_command',
            'some_var': 'not important',
        })

    @patch('team_sales_report.csv_operations.logger.error')
    @patch('team_sales_report.csv_operations.logger.debug')
    def test_read_csv_input_file_with_headers_in_file(self, debug_mock, error_mock):
        test_header_name = 'teams'
        test_filename = 'tests/data/TeamMap.csv'
        expected = [{'TeamId': '1', 'Name': 'Fluffy Bunnies'}, {'TeamId': '2', 'Name': 'White Knights'}, {'TeamId': '3', 'Name': 'Kings and Queens'}]
        expected_debug = f'reading input data for "{test_header_name}" from "{test_filename}"'

        result = test_module.read_csv_input_file(test_filename, test_header_name)

        self.assertEqual(expected, result)
        debug_mock.assert_any_call(expected_debug)

    @patch('team_sales_report.csv_operations.map_header')
    @patch('team_sales_report.csv_operations.logger.error')
    @patch('team_sales_report.csv_operations.logger.debug')
    def test_read_csv_input_file_with_no_headers_missing_from_file(self, debug_mock, error_mock, map_mock):
        test_header_name = 'products'
        test_filename = 'tests/data/ProductMaster.csv'
        test_entry_1 = {'ProductId': '1', 'Name': 'Minor Widget', 'Price': '0.25', 'LotSize': '250'}
        test_entry_2 = {'ProductId': '2', 'Name': 'Critical Widget', 'Price': '5.00', 'LotSize': '10'}
        map_mock.side_effect = (test_entry_1, test_entry_2)

        expected = [
            test_entry_1,
            test_entry_2,
        ]
        expected_debug = f'reading input data for "{test_header_name}" from "{test_filename}"'

        result = test_module.read_csv_input_file(test_filename, test_header_name)

        self.assertEqual(expected, result)
        debug_mock.assert_any_call(expected_debug)

    @patch('team_sales_report.csv_operations.logger.error')
    def test_map_header_succeeds(self, error_mock):
        test_headers = ['header1', 'header2']
        test_values = [
            ['1', '1', ],
            ['2', '1', ],
        ]
        expected = {'header1': ['1', '1'], 'header2': ['2', '1']}

        result = test_module.map_header(test_values, test_headers)

        self.assertEqual(expected, result)

    @patch('team_sales_report.csv_operations.logger.error')
    def test_map_header_errors(self, error_mock):
        test_headers = ['header1']
        test_values = [
            ['1', '1', ],
            ['2', '1', ],
        ]
        expected = {'header1': ['1', '1'], 'header2': ['2', '1']}
        expected_error = f'did not find header for index 1 of data {test_values}'

        with self.assertRaises(SystemExit) as raised:
            test_module.map_header(test_values, test_headers)

        self.assertEqual(raised.exception.code, 1)
        error_mock.assert_called_once_with(expected_error)

    @patch('team_sales_report.csv_operations.logger.info')
    def test_write_json_to_csv_file(self, info_mock):
        test_target_file = 'tests/data/output/test_write_csv_output.csv'
        test_data = [
            {'Name': 'Minor Widget', 'GrossRevenue': '125.0', 'TotalUnits': '11', 'DiscountCost': '0.0'},
            {'Name': 'Critical Widget', 'GrossRevenue': '50.0', 'TotalUnits': '5', 'DiscountCost': '5.0'},
        ]

        expected_message = f'writing to {test_target_file}'

        test_module.write_json_to_csv_file(test_target_file, test_data)

        # read the written test data
        with open(test_target_file) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            result = list(csv.DictReader(csv_file, delimiter=','))

        self.assertEqual(test_data, result)
        info_mock.assert_called_once_with(expected_message)
