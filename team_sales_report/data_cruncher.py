"""Crunch data related to report calculations."""

import logging
from team_sales_report.csv_operations import read_csv_input_file
from team_sales_report.csv_operations import write_json_to_csv_file


logger = logging.getLogger(__name__)


def crunch_reports(args) -> None:
    """Process input data and invoke report operations."""
    # read file contents
    team_data = read_csv_input_file(args.t, 'teams')
    sales_data = read_csv_input_file(args.s, 'sales')
    product_data = read_csv_input_file(args.p, 'products')

    # map product and sales data together to make later operations simpler
    # modifies sales_data as a side effect
    map_product_data_to_sales_data(product_data, sales_data)

    # handle team data crunching and report
    team_report = report_by_team(team_data, sales_data)
    write_json_to_csv_file(args.team_report, team_report)

    # handle product data crunching and report
    product_report = report_by_product(sales_data)
    write_json_to_csv_file(args.product_report, product_report)


def map_product_data_to_sales_data(product_data: list, sales_data: list) -> None:
    """
    Calculate extended sales data using product data.

    This function modifies sales_data as a side effect
    """
    # map product data by id to make later lookups simpler
    mapped_product_data = {}
    for entry in product_data:
        mapped_product_data[entry.pop('ProductId')] = entry

    # combine sales and product data to make calculation logic simpler
    for entry in sales_data:
        # collect initial data from current_product_data
        product_id = entry.get("ProductId")
        current_product_data = mapped_product_data.get(product_id)
        entry["LotSize"] = current_product_data.get("LotSize")
        entry["Price"] = current_product_data.get("Price")
        entry["Name"] = current_product_data.get("Name")

        # calculate extended price and discount data
        entry["LotPrice"] = int(entry.get("LotSize")) * float(entry.get("Price"))
        # uses previously calculated value
        entry["ExtPrice"] = float(entry.get("LotPrice")) * float(entry.get("Quantity"))
        # uses previously calculated value
        # Discount is given as a percentage, so / 100.0
        entry["ExtDiscount"] = float(entry.get("ExtPrice")) * float(entry.get("Discount")) / 100.0


def report_by_team(team_data: list, sales_data: list) -> list:
    """Calculate output report by team."""
    output_data = []

    for team in team_data:
        team_id = team.get('TeamId')
        team_name = team.get('Name')

        # gets all sales entries for current team
        sales_aggregate = list(map(
            lambda x: float(x.get('ExtPrice')) if x.get('TeamId') == team_id else 0,
            sales_data
        ))

        # sums sales entries for current team and creates output structure
        team_data = {
            'Team': team_name,
            # convert float to string and format with 2 decimal places (money)
            'GrossRevenue': f'{sum(sales_aggregate):.2f}',
        }

        output_data.append(team_data)

    return output_data


def report_by_product(sales_data: list) -> list:
    """Calculate output report by product."""
    product_report = {}
    # make base report structure
    # some entries of product_report may get updated multiple times
    # setting up the base structure will avoid the need to check for key errors
    # while updating the output data
    for entry in sales_data:
        product_report[entry.get('Name')] = {
            'Name': entry.get('Name'),
            'GrossRevenue': 0.0,
            'TotalUnits': 0,
            'DiscountCost': 0.0,
        }

    # update report with sales data
    for entry in sales_data:
        product_name = entry.get('Name')
        # no need for a try/except KeyError here since the report structure is controlled above
        current_output = product_report.get(product_name)

        # update values
        # ExtPrice and ExtDiscount already account for multiplication
        new_revenue = current_output.get('GrossRevenue') + float(entry.get('ExtPrice'))
        new_total_units = current_output.get('TotalUnits') + int(entry.get('Quantity'))
        new_discount_cost = current_output.get('DiscountCost') + float(entry.get('ExtDiscount'))

        # update values in product_report
        current_output['GrossRevenue'] = new_revenue
        current_output['TotalUnits'] = new_total_units
        current_output['DiscountCost'] = new_discount_cost

    # convert to a list of dicts for simpler write
    output_data = list(product_report.values())

    # convert floats to strings and format with 2 decimal places (money)
    for entry in output_data:
        entry['GrossRevenue'] = f'{entry.get("GrossRevenue"):.2f}'
        entry['DiscountCost'] = f'{entry.get("DiscountCost"):.2f}'

    return output_data
