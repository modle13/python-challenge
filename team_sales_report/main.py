"""Main entry point for Team Sales Report."""

import argparse
import logging

from team_sales_report.data_cruncher import crunch_reports

logger = logging.getLogger(__name__)


def run():
    """Entry point for Main script."""
    args = parse_args()
    configure_logging(args)
    crunch_reports(args)


def parse_args():
    """Convert command line input vars to an argparse object."""
    parser = define_parser()
    args = parser.parse_args()
    return args


def define_parser():
    """Set up argparse configuration."""
    parser = argparse.ArgumentParser(description="Report team sales performance.")
    parser.add_argument('--debug', action='store_true', help="Display debug logging.")
    parser.add_argument(
        '-t',
        metavar='TEAM_MAP',
        type=str,
        required=True,
        help="Team ID mapping file."
    )
    parser.add_argument(
        '-p',
        metavar='PRODUCT_MASTER',
        type=str,
        required=True,
        help="Product data file."
    )
    parser.add_argument(
        '-s',
        metavar='SALES',
        type=str,
        required=True,
        help="Sales data file."
    )
    parser.add_argument(
        '--team-report',
        metavar='TEAM_REPORT',
        type=str,
        required=True,
        help="Team report output file."
    )
    parser.add_argument(
        '--product-report',
        metavar='PRODUCT_REPORT',
        type=str,
        required=True,
        help="Product report output file."
    )
    return parser


def configure_logging(args):
    """Set up logger."""
    logging.basicConfig(
        level=(logging.DEBUG if args.debug else logging.INFO),
        format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
        datefmt='%Y-%m-%d:%H:%M:%S'
    )


if __name__ == '__main__':
    run()
