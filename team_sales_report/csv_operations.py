"""This module handles csv file operations."""

import csv
import json
import logging
import os

logger = logging.getLogger(__name__)


"""Known input data file header configuration when not provided in input file."""
header_mapping = {
    'products': ['ProductId', 'Name', 'Price', 'LotSize', ],
    'sales': ['SaleId', 'ProductId', 'TeamId', 'Quantity', 'Discount', ],
    'teams': [],
}


def read_csv_input_file(filename: str, header_name: str) -> list:
    """
    Read and return csv file contents.

    An alternate approach for handling headers would be to use csv.Sniffer(),
    but since header presense of input files is known, no need to determine
    header presence dynamically at this time.
    """
    logger.debug(f'reading input data for "{header_name}" from "{filename}"')

    if not os.path.exists(filename):
        logger.error(f'expected file but did not find it: {filename}')
        exit(1)

    with open(filename) as csv_file:
        header = header_mapping.get(header_name)
        if header:
            # this is a headerless input file for which the mapping is known
            csv_reader = csv.reader(csv_file, delimiter=',')
            entries = []
            for entry in csv_reader:
                mapped_entry = map_header(entry, header)
                entries.append(mapped_entry)
        else:
            # files with headers are not mapped
            # this can result in mangled data if accidentally reading a non-headed file with DictReader
            entries = list(csv.DictReader(csv_file, delimiter=','))

    logger.debug(json.dumps(entries, indent=4, sort_keys=True))
    return entries


def map_header(values: list, header: list) -> dict:
    """
    Apply header values to data and convert to dict.

    There might be a csv built-in that will do this.
    """
    mapped_values = {}

    for index, value in enumerate(values):
        try:
            column_name = header[index]
        except IndexError:
            logger.error(f'did not find header for index {index} of data {values}')
            # since input structure is known, this would be a configuration error
            exit(1)

        mapped_values[column_name] = value

    return mapped_values


def write_json_to_csv_file(target_file: str, data: list):
    """
    Write provided data to target file.

    Always expects a list of dictionaries.
    """
    logger.info(f'writing to {target_file}')
    with open(target_file, 'w') as output_file:
        csv_writer = csv.writer(output_file)
        count = 0
        for entry in data:
            if count == 0:
                # grab header from first entry
                csv_writer.writerow(list(entry.keys()))
                count += 1
            # add values to output
            csv_writer.writerow(list(entry.values()))
