# Install

Execution of this project only uses core modules and does not require a virtualenv.

Python3 is required. Verified with python3.8 and python3.9.

Tests, linting, and packaging do require third-party libraries.

# Running the report

See execution help:

```
python3 report.py --help
```

Execute the report directly from source:
```
python3 report.py \
    -t TeamMap.csv \
    -p ProductMaster.csv \
    -s Sales.csv \
    --team-report=TeamReport.csv \
    --product-report=ProductReport.csv
```

Alternate execution methods:

```
# use the binary script entry point
# replace `python3 report.py` above with:
bin/report

# use the helper script
./run.sh

# build and install the whl (see "Package the application", then execute from a venv
# replace `python3 report.py` above with:
report
```

Sample output from whl install:

```
(venv) # pip install Team_Sales_Report-0.1.0-py3-none-any.whl 
Processing ./Team_Sales_Report-0.1.0-py3-none-any.whl
Installing collected packages: Team-Sales-Report
Successfully installed Team-Sales-Report-0.1.0
(venv) # pip list
Package           Version
----------------- -------
pip               20.0.2 
pkg-resources     0.0.0  
setuptools        44.0.0 
Team-Sales-Report 0.1.0 
(venv) # report     -t TeamMap.csv     -p ProductMaster.csv     -s Sales.csv     --team-report=TeamReport.csv     --product-report=ProductReport.csv
2021-10-26:08:59:03,861 INFO     [csv_operations.py:79] writing to TeamReport.csv
2021-10-26:08:59:03,861 INFO     [csv_operations.py:79] writing to ProductReport.csv
(venv) # ls
ProductMaster.csv  ProductReport.csv  Sales.csv  TeamMap.csv  TeamReport.csv  Team_Sales_Report-0.1.0-py3-none-any.whl  venv

```

# Testing the source

##  Set up the test environment

```
# set up and activate test venv
python3 -m venv venv
. venv/bin/activate
# install test dependencies
pip install -r requirements/requirements-test.txt
```

## Execute the engineering tests

Note: requires test requirements installation

```
# run tests
chmod +x test.sh
./test.sh
```

sample test output:

```
========================================= test session starts =========================================
platform linux -- Python 3.8.5, pytest-6.2.5, py-1.10.0, pluggy-1.0.0
rootdir: /home/user/xr-coding-challenge
plugins: cov-3.0.0
collected 12 items

tests/unit/test_csv_operations.py .....
tests/unit/test_data_cruncher.py ....
tests/unit/test_main.py ...

------------- generated xml file: /home/user/xr-coding-challenge/test-report.xml --------------

----------- coverage: platform linux, python 3.8.5-final-0 -----------
Name                                  Stmts   Miss Branch BrPart  Cover   Missing
---------------------------------------------------------------------------------
team_sales_report/__init__.py             0      0      0      0   100%
team_sales_report/csv_operations.py      42      2     12      1    94%   30-31
team_sales_report/data_cruncher.py       49      0     12      0   100%
team_sales_report/main.py                25      2      2      1    89%   62, 77
---------------------------------------------------------------------------------
TOTAL                                   116      4     26      2    96%
Coverage XML written to file coverage.xml

Required test coverage of 90% reached. Total coverage: 95.77%
========================================== 12 passed in 0.16s ==========================================
```

## Execute the flake8 linter

Note: requires test requirements installation

```
# run linter; no output means linting passes
# see `setup.cfg` > `flake8` > `extend-ignore` for a list of linter exclusions
flake8
```

## Package the application

Note: requires test requirements installation

```
# generates a whl package at dist/: dist/Team_Sales_Report-0.1.0-py3-none-any.whl
# version and name are configured in setup.py
python3 setup.py bdist_wheel
```
