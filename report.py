#!/usr/bin/env python

"""
Entry point script for Team Sales Report.

Do not change this file.
This file MUST remain simple, as it is not covered by unit tests.
"""

from team_sales_report.main import run

if __name__ == '__main__':
    run()
