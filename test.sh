export PYTHONPATH=$PYTHONPATH:.

# note: these operations can be combined into a tool like tox

flake8

pytest -s --cov=team_sales_report/ \
       --cov-report=term-missing --cov-report=xml:coverage.xml --junitxml=test-report.xml \
       --cov-branch --cov-fail-under=90

# uncomment to execute build
#python setup.py bdist_wheel
